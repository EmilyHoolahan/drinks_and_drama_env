import Search from "./search";
import { FaTrash } from "react-icons/fa";
import { ImUndo2 } from "react-icons/im";
import useCards from "../../hooks/api_card";
import { useState } from "react";
import { Card } from "../../types/card";

const deck_builder = () => {
  const { data } = useCards();
  const [library, setLibrary] = useState<Card[]>([]);
  const [chosenCards, setChosenCards] = useState<Card[]>([]);

  const moveCardToChosen = (card: Card) => {
    // Remove the card from the library
    const updatedLibrary = library.filter((c) => c.card_id !== card.card_id);
    setLibrary(updatedLibrary);

    // Add the card to the chosen container
    setChosenCards([...chosenCards, card]);
  };
  const removeCardFromChosen = (index: number) => {
    const updatedChosenCards = chosenCards.filter((_, i) => i !== index);
    setChosenCards(updatedChosenCards);
  };
  const emptyChosenCards = () => {
    // Remove all chosen cards back to the library
    setLibrary((prevLibrary) => [...prevLibrary, ...chosenCards]);
    setChosenCards([]);
  };

  return (
    <>
      <div className="sticky_nav left_sticy_nav">
        <div className="container">
          <p> </p>
          {/* <div className="filter_element">
                    <img src="./img/gems/group.webp" alt="all" />
                    <p className="smol">All</p>
                </div>
                <div className="filter_element">
                    <img src="./img/gems/t.webp" alt="truth" />
                    <p className="smol">Truth</p>
                </div>
                <div className="filter_element">
                    <img src="./img/gems/dare.webp" alt="dare" />
                    <p className="smol">Dare</p>
                </div>
                <div className="filter_element">
                    <img src="./img/gems/dare.webp" alt="group dare" />
                    <p className="smol">Group Dare</p>
                </div>
                <div className="filter_element">
                    <img src="./img/gems/t.webp" alt="group truth" />
                    <p className="smol">Group Truth</p>
                </div>
                <div className="filter_element">
                    <img src="./img/gems/trap.webp" alt="trap cards" />
                    <p className="smol">Trap Cards</p>
                </div>
                <div className="filter_element">
                    <img src="./img/gems/drink.webp" alt="drink" />
                    <p className="smol">Drink</p>
                </div> */}
        </div>
      </div>

      <div className="build_deck_library_container">
        <div className="build_deck_left">
          <div>
            <Search />
            <div className="deck_colaps_container">
              <div className="gold_lines_container">
                <div className="gold_lines"></div>
                <h2>Prototype Deck</h2>
                <div className="gold_lines"></div>
              </div>

              <div className="library">
                {data &&
                  data.map((card, index) => (
                    <div
                      className="cards_in_libary"
                      key={index}
                      onClick={() => moveCardToChosen(card)}
                    >
                      <img src={"./img/cards/" + card.card_img} alt="card" />
                    </div>
                  ))}
              </div>
            </div>
            <button type="button" className="button_2">
              <p>Hide Cards</p>
            </button>
          </div>
        </div>
        <div className="built_deck_right">
          <input type="text" placeholder="Deck Name" />
          <div className="card_count_container">
            <div className="filter_element">
              <img src="./img/gems/t.webp" alt="truth" />
              <p>0</p>
            </div>
            <div className="filter_element">
              <img src="./img/gems/dare.webp" alt="dare" />
              <p>0</p>
            </div>
            <div className="filter_element">
              <img src="./img/gems/dare.webp" alt="group dare" />
              <p>0</p>
            </div>
            <div className="filter_element">
              <img src="./img/gems/t.webp" alt="group truth" />
              <p>0</p>
            </div>
            <div className="filter_element">
              <img src="./img/gems/trap.webp" alt="trap cards" />
              <p>0</p>
            </div>
            <div className="filter_element">
              <img src="./img/gems/drink.webp" alt="drink" />
              <p>0</p>
            </div>
          </div>
          <div className="edit_deck">
            <div className="empty_deck" onClick={emptyChosenCards}>
              <FaTrash />
              <p>Empty</p>
            </div>
            <div>
              <p>Undo</p>
              <ImUndo2 />
            </div>
          </div>
          <div className="gold_lines"></div>
          <div className="chosen_container">
            {chosenCards.map((card, index) => (
              <div
                className="chosen_cards"
                onClick={() => removeCardFromChosen(index)}
                key={index}
              >
                <div>
                  <h4>{card.card_title}</h4>
                  <p>{card.card_description}</p>
                </div>
                <img src={"./img/cards/" + card.card_img} alt="card" />
              </div>
            ))}
          </div>
          <button type="button" className="button_1">
            <p>Save Deck</p>
          </button>
        </div>
      </div>
    </>
  );
};

export default deck_builder;
