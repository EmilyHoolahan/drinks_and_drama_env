"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const winston_1 = require("winston");
const logger = (0, winston_1.createLogger)({
  level: "info",
  format: winston_1.format.combine(
    winston_1.format.timestamp(),
    winston_1.format.json()
  ),
  transports: [
    new winston_1.transports.File({
      filename: "./logs/combined.log",
    }),
    new winston_1.transports.File({
      filename: "./logs/info.log",
      level: "info",
      maxsize: 1024 * 1024,
    }),
    new winston_1.transports.File({
      filename: "./logs/error.log",
      level: "error",
      maxsize: 1024 * 1024,
    }),
    new winston_1.transports.File({
      filename: "./logs/silly.log",
      level: "silly",
      maxsize: 1024 * 1024,
    }),
    new winston_1.transports.File({
      filename: "./logs/verbose.log",
      level: "verbose",
      maxsize: 1024 * 1024,
    }),
    new winston_1.transports.Console(),
  ],
});
exports.default = logger;
