
export interface User {
    user_id: number;
    user_username: string;
    user_password: string;
    user_email: string;
    user_img: number;
}