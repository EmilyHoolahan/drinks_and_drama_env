import axios from "axios";
import Cookies from "js-cookie";
import { useUser } from "../../hooks/api_signup";
import { useState } from "react";

const Login = (props2: any) => {
  const handleModuleNavigation2 = () => {
    props2.currentNav("signup");
  };

  const [data, setData] = useState({
    user_username: "",
    user_password: "",
  });

  const { dispatch } = useUser();

  const handleChange = (e: any) => {
    const { name, value } = e.target;
    setData({
      ...data,
      [name]: value,
    });
  };

  const handleSubmit = async (e: any) => {
    e.preventDefault();
    const userData = {
      user_username: data.user_username,
      user_password: data.user_password,
    };
    try {
      const response = await axios.post(
        "http://app-backend:3000/auth/login",
        userData
      );
      console.log("Axius response:", response);

      dispatch({ type: "LOGIN", user: response.data.user });
      Cookies.set("authToken", response.data.authToken);
    } catch (error) {
      console.error("Axios error:", error);
    }
  };

  return (
    <>
      <h2>Welcome Back</h2>
      <div>
        <form>
          {" "}
          <input
            type="text"
            placeholder="Username"
            name="user_username"
            value={data.user_username}
            onChange={handleChange}
          />
          <input
            type="password"
            placeholder="Password"
            name="user_password"
            value={data.user_password}
            onChange={handleChange}
          />
          <div className="checkbox_container">
            <input type="checkbox" name="remember" />
            <label htmlFor="remember">Remember Me</label>
          </div>
          <button type="button" className="button_1" onClick={handleSubmit}>
            <p>Login</p>
          </button>
          <button
            type="button"
            className="button_2"
            onClick={() => {
              handleModuleNavigation2();
            }}
          >
            <p>Sign Up</p>
          </button>
        </form>
      </div>
    </>
  );
};
export default Login;
