import "./app.scss";
import Footer from "./components/navigation/footer";
import Fireflies from "./components/home/fireflies";
import Home from "./components/home/home";
import Cards from "./components/cards-decks/cards";
import { UserProvider } from "./hooks/api_signup";
import { useState } from "react";
import Module from "./components/user/module";
import { QueryClient, QueryClientProvider } from "react-query";
const queryClient = new QueryClient();

function App() {
  const [navigation, setNavigation] = useState("Home");

  /* const handleNavigation = (props: any) => {
    setNavigation(props)
  } */
  const [visible, setVisible] = useState(true);
  const [userProfile, setUserProfile] = useState("");

  const handleUserProfile = (props: any) => {
    setUserProfile(props);
  };

  return (
    <UserProvider>
      <QueryClientProvider client={queryClient}>
        <header>
          <nav>
            <div className="flex">
              <a
                onClick={() => {
                  setNavigation("Home");
                }}
              >
                <img src="../../../public/img/logo_gold.webp" alt="logo" />
              </a>
              <a>
                <button type="button" className="play_now">
                  Play Now
                </button>
              </a>
            </div>
            <div className="second_nav">
              <a
                onClick={() => {
                  setNavigation("Cards");
                }}
              >
                Cards Decks & Shop
              </a>
            </div>
            <div className="flex">
              <div>
                <svg
                  width="31"
                  height="31"
                  viewBox="0 0 31 31"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M28.5889 31L17.7389 20.15C16.8778 20.8389 15.8875 21.3843 14.7681 21.7861C13.6486 22.188 12.4574 22.3889 11.1944 22.3889C8.06574 22.3889 5.41811 21.305 3.25156 19.1373C1.085 16.9696 0.00114815 14.322 0 11.1944C0 8.06574 1.08385 5.41811 3.25156 3.25156C5.41926 1.085 8.06689 0.00114815 11.1944 0C14.3231 0 16.9708 1.08385 19.1373 3.25156C21.3039 5.41926 22.3877 8.06689 22.3889 11.1944C22.3889 12.4574 22.188 13.6486 21.7861 14.7681C21.3843 15.8875 20.8389 16.8778 20.15 17.7389L31 28.5889L28.5889 31ZM11.1944 18.9444C13.3472 18.9444 15.1774 18.1907 16.6849 16.6832C18.1924 15.1756 18.9456 13.3461 18.9444 11.1944C18.9444 9.04167 18.1907 7.21152 16.6832 5.704C15.1756 4.19648 13.3461 3.4433 11.1944 3.44444C9.04167 3.44444 7.21152 4.1982 5.704 5.70572C4.19648 7.21324 3.4433 9.04281 3.44444 11.1944C3.44444 13.3472 4.1982 15.1774 5.70572 16.6849C7.21324 18.1924 9.04281 18.9456 11.1944 18.9444Z"
                    fill="black"
                    fillOpacity="0.5"
                  />
                </svg>
              </div>
              <div
                onClick={() => {
                  setVisible(!visible);
                }}
              >
                <svg
                  width="31"
                  height="31"
                  viewBox="0 0 31 31"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M15.5 0C17.5554 0 19.5267 0.816515 20.9801 2.26992C22.4335 3.72333 23.25 5.69457 23.25 7.75C23.25 9.80543 22.4335 11.7767 20.9801 13.2301C19.5267 14.6835 17.5554 15.5 15.5 15.5C13.4446 15.5 11.4733 14.6835 10.0199 13.2301C8.56652 11.7767 7.75 9.80543 7.75 7.75C7.75 5.69457 8.56652 3.72333 10.0199 2.26992C11.4733 0.816515 13.4446 0 15.5 0ZM15.5 19.375C24.0637 19.375 31 22.8431 31 27.125V31H0V27.125C0 22.8431 6.93625 19.375 15.5 19.375Z"
                    fill="black"
                    fillOpacity="0.5"
                  />
                </svg>
              </div>
              <div>
                <svg
                  width="31"
                  height="31"
                  viewBox="0 0 31 31"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M2 2H29ZM2 15.5H29ZM2 29H29Z"
                    fill="black"
                    fillOpacity="0.5"
                  />
                  <path
                    d="M2 2H29M2 15.5H29M2 29H29"
                    stroke="black"
                    strokeOpacity="0.5"
                    strokeWidth="4"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                </svg>
              </div>
            </div>
          </nav>
        </header>

        <section
          id="user"
          className={` ${visible ? "hide" : ""} ${
            userProfile === "user_profile" ? "add_scroll_overflow" : ""
          }`}
        >
          <Module onLogin={handleUserProfile} />
        </section>

        <main>
          <Fireflies />

          <div className={`${navigation === "Home" ? "show" : "hide"}`}>
            <Home />
          </div>

          <div className={`${navigation === "Cards" ? "show" : "hide"}`}>
            <Cards />
          </div>
        </main>

        <Footer />
      </QueryClientProvider>
    </UserProvider>
  );
}

export default App;
