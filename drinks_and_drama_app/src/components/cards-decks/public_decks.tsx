import Search from "./search"


const PublicDecks = () => {
    return (<>

        <div className="sticky_nav">
            <div className="container">
                <div className="filter_element">
                    <img src="./img/gems/group.webp" alt="all" />
                    <p className="smol">All</p>
                </div>
                <div className="filter_element">
                    <img src="./img/gems/t.webp" alt="truth" />
                    <p className="smol">Truth</p>
                </div>
                <div className="filter_element">
                    <img src="./img/gems/dare.webp" alt="dare" />
                    <p className="smol">Dare</p>
                </div>
                <div className="filter_element">
                    <img src="./img/gems/dare.webp" alt="group dare" />
                    <p className="smol">Group Dare</p>
                </div>
                <div className="filter_element">
                    <img src="./img/gems/t.webp" alt="group truth" />
                    <p className="smol">Group Truth</p>
                </div>
                <div className="filter_element">
                    <img src="./img/gems/trap.webp" alt="trap cards" />
                    <p className="smol">Trap Cards</p>
                </div>
                <div className="filter_element">
                    <img src="./img/gems/drink.webp" alt="drink" />
                    <p className="smol">Drink</p>
                </div>
                <div className="filter_element">
                    <img src="./img/gems/group.webp" alt="drink" />
                    <p className="smol">Total</p>
                </div>

            </div>
        </div>
        <Search />
        <section id="publick_card_library">
            <div className="container">
                <div>
                    <div className="gold_lines_container">
                        <div className="gold_lines"></div>
                        <h2>Username</h2>
                        <div className="gold_lines"></div>
                    </div>
                    <div className="library">
                        <img src="./img/card_back.webp" alt="card" />
                        <img src="./img/card_back.webp" alt="card" />
                        <img src="./img/card_back.webp" alt="card" />
                        <img src="./img/card_back.webp" alt="card" />
                        <img src="./img/card_back.webp" alt="card" />
                        <img src="./img/card_back.webp" alt="card" />
                        <img src="./img/card_back.webp" alt="card" />
                        <img src="./img/card_back.webp" alt="card" />
                        <img src="./img/card_back.webp" alt="card" />
                        <img src="./img/card_back.webp" alt="card" />
                    </div>
                </div>

            </div>
        </section>
    </>
    )

}

export default PublicDecks