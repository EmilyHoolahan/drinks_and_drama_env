

export default function CardsHome() {
    return (
        <>
            <section id="cards_home">
                <div className="container">
                    <div className="gold_lines_container">
                        <div className="gold_lines"></div>
                        <h2>Cards</h2>
                        <div className="gold_lines"></div>
                    </div>
                    <p className="descript"> Take a sneak peak into what tasks you can expect from Drinks & Drama
                        Or go straight to building your own deck, that you can play online free or buy to take home. </p>
                    <div id="cards_nav">
                        <a className="card_a">
                            <img src="./img/card_back.webp" alt="card_back" />
                            <div className="overlay">
                                <h3>All Cards</h3>
                            </div>
                        </a>
                        <a className="card_a">
                            <img src="./img/card_back.webp" alt="card_back" />
                            <div className="overlay">
                                <h3>Build Deck</h3>
                            </div>
                        </a>
                        <a className="card_a">
                            <img src="./img/card_back.webp" alt="card_back" />
                            <div className="overlay">
                                <h3>Buy Deck</h3>
                            </div>
                        </a>

                    </div>
                </div>
            </section>
        </>
    )
}