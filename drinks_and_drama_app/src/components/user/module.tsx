import { useState } from "react";
import Signup from "./signup";
import Login from "./login";
import UserProfile from "./user_profile";

type Props = {
  onLogin: (arg: string) => void;
};

export default function Module(props: Props) {
  const [moduleNavigation, setModuleNavigation] = useState("signup");

  const handleModuleNavigation = (props2: any) => {
    setModuleNavigation(props2);
    if (props2 === "user_profile") {
      // props.onLogin('user_profile')
      props.onLogin(props2);
    }
  };

  return (
    <>
      <div
        id="signup"
        className={`container ${
          moduleNavigation === "signup" ? "show" : "hide"
        }`}
      >
        <Signup currentNav={handleModuleNavigation} />
      </div>

      <div
        id="login"
        className={`container ${
          moduleNavigation === "login" ? "show" : "hide"
        }`}
      >
        <Login currentNav={handleModuleNavigation} />
      </div>

      <div
        id="user_profile"
        className={`container ${
          moduleNavigation === "user_profile" ? "show" : "hide"
        }`}
      >
        <UserProfile currentNav={handleModuleNavigation} />
      </div>
    </>
  );
}
