

export default function HowToPlay() {
    return (
        <>
            <section id="how_to_play">
                <div className="container">
                    <div className="gold_lines_container">
                        <div className="gold_lines"></div>
                        <h2>How to Play</h2>
                        <div className="gold_lines"></div>
                    </div>
                    <p className="descript">Simply by taking turns drawing a card and completing the task on the card
                        but beware of Trap Cards
                    </p>
                    <div id="htp_content_container">
                        <div className="left">
                            <img src="./img/gems/trap.webp" alt="how_to_play" />
                            <p>Trap Cards</p>
                        </div>
                        <div className="mid">
                            <div>
                                <h3>Da Rules</h3>
                                <p>If you draw a card that wont be fun for you to complete, then take a drink and either
                                    draw another card or pass your turn.

                                    Remember to do a vibe check, if someone does not want to participate in a group card,
                                    they drink instead.

                                    You decide how for you go to complete a task.
                                    You can be as vague or detailed as you feel comfortable with
                                </p>
                            </div>
                            <div><img src="./img/rules.webp" alt="da_rules" /></div>
                        </div>
                        <div className="right"><img src="./img/gems/dare.webp" alt="how_to_play" />
                            <p>Gems</p>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}