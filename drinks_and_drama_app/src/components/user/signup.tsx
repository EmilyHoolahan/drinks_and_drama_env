import axios from "axios";
import Cookies from "js-cookie";
import { useState } from "react";
import { useUser } from "../../hooks/api_signup";

const Signup = (props2: any) => {
  const handleModuleNavigation = () => {
    props2.currentNav("login");
  };

  const [data, setData] = useState({
    user_username_sig: "",
    user_password_sig: "",
    user_email_sig: "",
  });

  const { dispatch } = useUser();

  const handleChange = (e: any) => {
    const value = e.target.value;
    setData({
      ...data,
      [e.target.name]: value,
    });
  };

  const [errorMessage, setErrorMessage] = useState("");

  const validation = (data: any) => {
    if (!data.user_username_sig.trim()) {
      setErrorMessage("Where Username?");
    } else if (!/^[a-zA-Z]+$/.test(data.user_username_sig)) {
      setErrorMessage(
        "Only letters in Username, so no YourMom69!!!, you will have to live with YourMom"
      );
    } else if (!data.user_email_sig.trim()) {
      setErrorMessage("Where Email?");
    } else if (
      !/[a-z0-9!#$%&'+/=?^`{|}~-]+(?:.[a-z0-9!#$%&'+/=?^`{|}~-]+)@(?:[a-z0-9](?:[a-z0-9-][a-z0-9])?.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(
        data.user_email_sig
      )
    ) {
      setErrorMessage("Thats no email! You can't fool me!");
    } else if (!data.user_password_sig.trim()) {
      setErrorMessage("Where Password?");
    } else if (data.user_password_sig.length <= 6) {
      setErrorMessage("Your Password is weak! 5 characters needed! At least");
    } else {
      setErrorMessage("yaas queen");
    }
  };
  const handleSubmit = async (e: any) => {
    e.preventDefault();
    const userData = {
      user_username_sig: data.user_username_sig,
      user_password_sig: data.user_password_sig,
      user_email_sig: data.user_email_sig,
      //    takenUsername: data.takenUsername,
    };
    if (errorMessage == "yaas queen") {
      try {
        const response = await axios.post(
          "http://localhost:3000/auth/signup",
          userData
        );
        console.log("Axios response:", response);
        dispatch({ type: "LOGIN", user: response.data.user });
        Cookies.set("authToken", response.data.authToken);
        handleModuleNavigation();
        console.log(errorMessage);
      } catch (axiosError: any) {
        console.log("Axios Error:", axiosError);
      }
    } else {
      console.log("naaah queen");
      const value = e.target.value;
      setData({
        ...data,
        [e.target.name]: value,
      });
      validation(data);
    }
  };

  return (
    <>
      <h2>Create New Account</h2>
      <p>Is it - and always will be - Free</p>
      <div>
        <form>
          {/* <input type="text" name="takenUsername" value={data.takenUsername} onChange={handleChange} /> */}
          <input
            type="text"
            placeholder="Username"
            name="user_username_sig"
            value={data.user_username_sig}
            onChange={handleChange}
          />
          <input
            type="password"
            placeholder="Password"
            name="user_password_sig"
            value={data.user_password_sig}
            onChange={handleChange}
          />
          {/*     <input type="password" placeholder="Repeat Password" /> */}
          <input
            type="email"
            placeholder="Email"
            name="user_email_sig"
            value={data.user_email_sig}
            onChange={handleChange}
          />
          <div className="checkbox_container">
            <input type="checkbox" name="terms" />
            <label htmlFor="terms">
              Agree to Terms and other things you wont read
            </label>
          </div>
          <div className="checkbox_container">
            <input type="checkbox" name="age" />
            <label htmlFor="age">I am 18 years old or older</label>
          </div>
          <div>
            <p className="smol">{errorMessage}</p>
          </div>
          <button type="button" className="button_1" onClick={handleSubmit}>
            <p>Create</p>
          </button>
          <button
            type="button"
            className="button_2"
            onClick={() => {
              handleModuleNavigation();
            }}
          >
            <p>Login</p>
          </button>
        </form>
      </div>
    </>
  );
};
export default Signup;
