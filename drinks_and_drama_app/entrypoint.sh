#!/bin/sh



case "$RTE" in
    dev )
        echo "Dev mode"
        npm audit
        ;;
    test )
        echo "Test mode"
        npm audit || exit 1
        ;;
    prod )
        echo "Production mode"
        npm audit || exit 1
        npm run build || exit 1
        ;;
esac
