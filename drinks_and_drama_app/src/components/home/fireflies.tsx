export default function Fireflies() {
    return (
        <div id="fireflie_container_h1">
            <div id="fireflie_container" className="fly">
                <div id="unit" className="glow"></div>
            </div>
            <div id="fireflie_container2" className="fly2">
                <div id="unit2" className="glow2"></div>
            </div>
            <div id="fireflie_container3" className="fly3">
                <div id="unit3" className="glow3"></div>
            </div>
            <div id="fireflie_container4" className="fly4">
                <div id="unit4" className="glow4"></div>
            </div>
            <div id="fireflie_container5" className="fly5">
                <div id="unit5" className="glow5"></div>
            </div>
            <div id="fireflie_container6" className="fly6">
                <div id="unit6" className="glow6"></div>
            </div>

            <div id="fireflie_container7" className="fly7">
                <div id="unit7" className="glow7"></div>
            </div>
            <div id="fireflie_container8" className="fly8">
                <div id="unit8" className="glow8"></div>
            </div>
            <div id="fireflie_container9" className="fly9">
                <div id="unit9" className="glow9"></div>
            </div>
            <div id="fireflie_container10" className="fly10">
                <div id="unit10" className="glow10"></div>
            </div>

        </div>
    )
}