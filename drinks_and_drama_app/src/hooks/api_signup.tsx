import { createContext, useContext, useReducer, ReactNode } from "react";

interface UserContextProps {
  user: any; // You should replace 'any' with the actual type of your user object
  dispatch: (action: any) => void;
}

const UserContext = createContext<UserContextProps>({
  user: null,
  dispatch: (_action) => {},
});

export function useUser() {
  return useContext(UserContext);
}

interface UserProviderProps {
  children: ReactNode;
}


const initialState = {
  user: null,
};

export function UserProvider({ children }: UserProviderProps) {
  const [user, dispatch] = useReducer(userReducer, initialState);
  return (
    <UserContext.Provider value={{ user, dispatch }}>
      {children}
    </UserContext.Provider>
  );
}

function userReducer(state: any, action: any) {
  console.log("UserProvider state:", state, " action:", action);
  switch (action.type) {
    case "LOGIN":
      console.log("Dispatching LOGIN action:", action.user);
      return { ...state, user: action.user };
    case "LOGOUT":
      console.log("Dispatching LOGOUT action");
      return { ...state, user: null };
    default:
      return state;
  }
}
