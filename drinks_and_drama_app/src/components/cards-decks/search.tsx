const Search = () => {
    return (
        <div className="search_container">
            <div className="container">
                <input type="text" placeholder="Search" />
            </div>
        </div>
    )
}
export default Search