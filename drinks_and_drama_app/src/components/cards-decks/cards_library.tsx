import Search from "./search";
import apiCard from "../../hooks/api_card";

const cards_library = () => {
    const { data } = apiCard();
    return (<>
        <div className="sticky_nav">
            <div className="container">
                <div className="filter_element">
                    <img src="./img/gems/group.webp" alt="all" />
                    <p className="smol">All</p>
                </div>
                <div className="filter_element">
                    <img src="./img/gems/t.webp" alt="truth" />
                    <p className="smol">Truth</p>
                </div>
                <div className="filter_element">
                    <img src="./img/gems/dare.webp" alt="dare" />
                    <p className="smol">Dare</p>
                </div>
                <div className="filter_element">
                    <img src="./img/gems/dare.webp" alt="group dare" />
                    <p className="smol">Group Dare</p>
                </div>
                <div className="filter_element">
                    <img src="./img/gems/t.webp" alt="group truth" />
                    <p className="smol">Group Truth</p>
                </div>
                <div className="filter_element">
                    <img src="./img/gems/trap.webp" alt="trap cards" />
                    <p className="smol">Trap Cards</p>
                </div>
                <div className="filter_element">
                    <img src="./img/gems/drink.webp" alt="drink" />
                    <p className="smol">Drink</p>
                </div>
                <div className="filter_element">
                    <img src="./img/gems/group.webp" alt="drink" />
                    <p className="smol">Total</p>
                </div>

            </div>
        </div>
        <Search />
        <section id="card_library">
            <div className="container">
                <div>
                    <div className="gold_lines_container">
                        <div className="gold_lines"></div>
                        <h2>Prototype Deck</h2>
                        <div className="gold_lines"></div>
                    </div>
                    <div className="library">


                        {data && data.map((card, index) => (
                            <div key={card.card_id || index}>
                                <img src={"./img/cards/" + card.card_img} alt="card" />

                            </div>
                        ))}



                    </div>
                </div>
                <div>
                    <div className="gold_lines_container">
                        <div className="gold_lines"></div>
                        <h2>Family Deck</h2>
                        <div className="gold_lines"></div>
                    </div>
                    <h3>Coming Soon!</h3>
                </div>
                <div>
                    <div className="gold_lines_container">
                        <div className="gold_lines"></div>
                        <h2>Bar Deck</h2>
                        <div className="gold_lines"></div>
                    </div>
                    <h3>Coming Soon!</h3>
                </div>
            </div>
        </section>

    </>
    )

}

export default cards_library