import CardLibrary from "./cards_library"
import DeckBuilder from "./deck-builder"
import Shop from "../shop/shop"
import PublicDecks from "./public_decks"
import { useState } from "react"

const cards = () => {

    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [navigation, setNavigation] = useState("Cards Library")


    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [active, setActive] = useState("cards")



    return (<>
        <section className="hero">
            <div className="hero_container">
                <img src="./img/dd_big_logo.webp" alt="drinksanddrama" />
                <h2>{navigation}</h2>
            </div>
        </section>

        <nav className="second_non_sticy_nav">
            <div className="container">
                <a className={`${active === "cards" ? "active_card_nav" : ""}`} onClick={() => { setNavigation("Cards Library"); setActive("cards") }} >Cards</a>
                <a className={`${active === "deck builder" ? "active_card_nav" : ""}`} onClick={() => { setNavigation("Deck Builder"); setActive("deck builder") }}>Deck Builder</a>
                <a className={`${active === "shop" ? "active_card_nav" : ""}`} onClick={() => { setNavigation("Shop"); setActive("shop") }}>Shop</a>
                <a className={`${active === "publics deck" ? "active_card_nav" : ""}`} onClick={() => { setNavigation("Public Decks"); setActive("publics deck") }}>Public Decks</a>
            </div>
        </nav>
        <div className={`${navigation === "Cards Library" ? "show" : "hide"}`}>
            <CardLibrary />
        </div>
        <div className={`${navigation === "Deck Builder" ? "show" : "hide"}`}>
            <DeckBuilder />
        </div>
        <div className={`${navigation === "Shop" ? "show" : "hide"}`}>
            <Shop />
        </div>
        <div className={`${navigation === "Public Decks" ? "show" : "hide"}`}>
            <PublicDecks />
        </div>

    </>
    )

}

export default cards
