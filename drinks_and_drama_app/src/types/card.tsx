export interface Card {
    card_id: number;
    card_title: string;
    card_description: string;
    card_img: string;
    card_gem_id: number;
}