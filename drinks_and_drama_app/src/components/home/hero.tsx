import Logo from "../../../public/img/dd_big_logo.webp";
export default function Hero() {
    return (
        <>
            <section className="hero">
                <div className="hero_container">
                    <img src={Logo} alt="drinksanddrama" />
                    <h2>Make stronger connections while having all the fun</h2>
                    <p>Also it’s free, so why not?</p>
                    <button type="button" className="play_now">Play Now</button>
                </div>
            </section>
        </>
    )
}