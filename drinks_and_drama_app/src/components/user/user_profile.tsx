import Deck from "../../../public/img/card_back.webp";
import { IoColorWandSharp } from "react-icons/io5";
import { FaEyeSlash } from "react-icons/fa";
import { FaEye } from "react-icons/fa";
import { useState } from "react";
/* import useUser from "../../hooks/api_user"; */

type Props = {
  currentNav: (nav: string) => void;
};

const UserProfile = (props2: Props) => {
  /* const { data } = useUser(); */
  /*  console.log(data) */
  const handleModuleNavigation = () => {
    props2.currentNav("login");
  };

  const [publicto, setPublicto] = useState(false);
  return (
    <>
      {/* {data && data.map((user, index) => (
            <div key={user.user_id || index}>
                <h2>{user.user_username}</h2>

            </div>
        ))} */}

      <div id="user_profilepicture_container">
        <img
          id="user_profile_picture"
          src="./img/user_img/emily.webp"
          alt="profile_picture"
        />
        <IoColorWandSharp />
      </div>

      <div className="scroll_container">
        <div id="my_deck_top">
          <h3>My Decks</h3>

          <div
            className={`${publicto ? "hide" : "flex"}`}
            onClick={() => {
              setPublicto(!publicto);
            }}
          >
            <p>Hidden from other users</p>
            <FaEyeSlash />
          </div>

          <div
            className={`${publicto ? "flex" : "hide"}`}
            onClick={() => {
              setPublicto(!publicto);
            }}
          >
            <p>Public to other users</p>
            <FaEye />
          </div>
        </div>
        <div className="users_decks scroll"></div>
      </div>
      <div className="scroll_container">
        <h3>My Order History</h3>
        <div className="users_orders scroll">
          <img src={Deck} alt="" />
        </div>
      </div>
      <div id="settings">
        <h3>Settings</h3>
        <form>
          <input type="text" placeholder="Username" />
          <input type="password" placeholder="Password" />
          <input type="password" placeholder="Change Password" />
          <input type="email" placeholder="Email" />
          <div className="checkbox_container">
            <input type="checkbox" name="user_news" />
            <label htmlFor="user_news">Want news on Drinks & Drama?</label>
          </div>
          <button type="button" className="button_1">
            <p>Save Changes</p>
          </button>
        </form>
        <button
          type="button"
          className="button_1"
          onClick={() => {
            handleModuleNavigation();
          }}
        >
          <p>Log Out</p>
        </button>
      </div>
    </>
  );
};
export default UserProfile;
