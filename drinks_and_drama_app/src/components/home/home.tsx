import Hero from "./hero";
import CardsHome from "./cards_home";
import HowToPlay from "./how_to_play";
import Lore from "./lore";
import Community from "./community";


export default function Home() {
    return (
        <>
            <Hero />
            <CardsHome />
            <HowToPlay />
            <Lore />
            <Community />
        </>
    );
}
