import { useQuery } from "react-query";
import ApiClient, { FetchResponse } from "../service/apiClient";
import { Card } from "../types/card";

const apiClient = new ApiClient<Card>("/cards");
const apiCard = () => {
    return useQuery<FetchResponse<Card>, Error>('cards', () => apiClient.getAll());
}
export default apiCard;